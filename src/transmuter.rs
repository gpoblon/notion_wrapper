use std::str::FromStr;

use notion::{
    ids::PageId,
    models::{
        properties::{FormulaResultValue, PropertyValue, RollupValue},
        Page,
    },
};
use serde::{Deserialize, Serialize};
use serde_json::Number;
use validator::Validate;

use crate::result::*;

use super::{Api, Table, TableOutput, Tables};

/* allows for transmuting notion complex structure to simple types (i64, f64, String) */
// TODO add a method for vecs
pub trait PropertiesTrait {
    fn as_text_array(&self) -> AppResult<Vec<String>>;
    fn as_text(&self) -> AppResult<String>;
    fn as_f64(&self) -> AppResult<f64>;
    fn as_i64(&self) -> AppResult<i64>;
    fn as_u64(&self) -> AppResult<u64>;
    fn as_bool(&self) -> AppResult<bool>;
    fn as_select<E>(&self) -> AppResult<E>
    where
        E: Copy + FromStr;
    fn as_multi_select<E>(&self) -> AppResult<Vec<E>>
    where
        E: Copy + FromStr;
    fn as_relation(&self) -> AppResult<PageId>;
    fn as_relations(&self) -> AppResult<Vec<PageId>>;
    // could be page or directly parsed struct
    fn as_relations_pages(&self, pages: &Vec<Page>) -> AppResult<Vec<Page>>;

    fn get_type_str(&self) -> String;

    fn err_prop_type(&self, expected_type: &str) -> AppErrors {
        err!(AppErr::Value(format!(
            "critical: expected {expected_type} property, found {}",
            self.get_type_str()
        )))
    }
}

impl PropertiesTrait for PropertyValue {
    fn get_type_str(&self) -> String {
        match self {
            Self::Title { id, .. } => id,
            Self::Text { id, .. } => id,
            Self::Number { id, .. } => id,
            Self::Select { id, .. } => id,
            Self::MultiSelect { id, .. } => id,
            Self::Date { id, .. } => id,
            Self::Formula { id, .. } => id,
            Self::Relation { id, .. } => id,
            Self::Rollup { id, .. } => id,
            Self::People { id, .. } => id,
            Self::Files { id, .. } => id,
            Self::Checkbox { id, .. } => id,
            Self::Url { id, .. } => id,
            Self::Email { id, .. } => id,
            Self::PhoneNumber { id, .. } => id,
            Self::CreatedTime { id, .. } => id,
            Self::CreatedBy { id, .. } => id,
            Self::LastEditedTime { id, .. } => id,
            Self::LastEditedBy { id, .. } => id,
            Self::Status { id, .. } => id,
        }
        .to_string()
    }

    fn as_text_array(&self) -> AppResult<Vec<String>> {
        let texts = match self {
            PropertyValue::Title { title, .. } => title,
            PropertyValue::Text { rich_text, .. } => rich_text,
            PropertyValue::Formula {
                formula: FormulaResultValue::String { string },
                ..
            } => {
                return match string {
                    Some(s) => Ok(vec![s.to_owned()]),
                    None => Err(err!(AppErr::Value(
                        "found Formula but its either null or not a text computation".to_owned()
                    ))),
                }
            }
            _ => return Err(self.err_prop_type("[text] (array)")),
        };
        let res = texts
            .iter()
            .map(|text| text.plain_text().to_owned())
            .collect::<Vec<String>>();
        match res.is_empty() {
            false => Ok(res),
            true => Err(err!(AppErr::Value("found empty Text|Title".to_owned()))),
        }
    }

    fn as_text(&self) -> AppResult<String> {
        let texts = match self {
            PropertyValue::Title { title, .. } => title,
            PropertyValue::Text { rich_text, .. } => rich_text,
            PropertyValue::Formula {
                formula: FormulaResultValue::String { string },
                ..
            } => {
                return match string {
                    Some(s) => Ok(s.to_owned()),
                    None => Err(err!(AppErr::Value(
                        "found Formula but its either null or not a text computation".to_owned()
                    ))),
                }
            }
            _ => return Err(self.err_prop_type("text")),
        };
        Ok(texts
            .iter()
            .map(|text| text.plain_text())
            .collect::<Vec<&str>>()
            .concat())
    }

    fn as_f64(&self) -> AppResult<f64> {
        let number: Number = match self {
            PropertyValue::Number { number, .. } => number
                .to_owned()
                .ok_or_else(|| err!(AppErr::Value("found empty number".to_owned())))?,
            PropertyValue::Formula {
                formula: FormulaResultValue::Number { number },
                ..
            } => number.to_owned().ok_or_else(|| {
                err!(AppErr::Value(
                    "found Formula but its either null or not a number computation".to_owned()
                ))
            })?,
            PropertyValue::Rollup {
                rollup: Some(RollupValue::Number { number }),
                ..
            } => number.to_owned().ok_or_else(|| {
                err!(AppErr::Value(
                    "found Rollup but it's either null or not a number".to_owned()
                ))
            })?,
            _ => return Err(self.err_prop_type("f64")),
        };
        number
            .as_f64()
            .ok_or_else(|| err!(AppErr::Value("number is not a valid float".to_owned())))
    }

    fn as_i64(&self) -> AppResult<i64> {
        let number: Number = match self {
            PropertyValue::Number { number, .. } => number
                .to_owned()
                .ok_or_else(|| err!(AppErr::Value("found empty number".to_owned())))?,
            PropertyValue::Formula {
                formula: FormulaResultValue::Number { number },
                ..
            } => number.to_owned().ok_or_else(|| {
                err!(AppErr::Value(
                    "found Formula but its either null or not a number computation".to_owned()
                ))
            })?,
            PropertyValue::Rollup {
                rollup: Some(RollupValue::Number { number }),
                ..
            } => number.to_owned().ok_or_else(|| {
                err!(AppErr::Value(
                    "found Rollup but it's either null or not a number".to_owned()
                ))
            })?,
            _ => return Err(self.err_prop_type("i64")),
        };
        number
            .as_i64()
            .ok_or_else(|| err!(AppErr::Value("number is not a valid integer".to_owned())))
    }

    fn as_u64(&self) -> AppResult<u64> {
        let number: Number = match self {
            PropertyValue::Number { number, .. } => number
                .to_owned()
                .ok_or_else(|| err!(AppErr::Value("found empty number".to_owned())))?,
            PropertyValue::Formula {
                formula: FormulaResultValue::Number { number },
                ..
            } => number.to_owned().ok_or_else(|| {
                err!(AppErr::Value(
                    "found Formula but its either null or not a number computation".to_owned()
                ))
            })?,
            PropertyValue::Rollup {
                rollup: Some(RollupValue::Number { number }),
                ..
            } => number.to_owned().ok_or_else(|| {
                err!(AppErr::Value(
                    "found Rollup but it's either null or not a number".to_owned()
                ))
            })?,
            _ => return Err(self.err_prop_type("u64")),
        };
        number.as_u64().ok_or_else(|| {
            err!(AppErr::Value(
                "number is not a valid unsigned integer".to_owned()
            ))
        })
    }

    fn as_bool(&self) -> AppResult<bool> {
        match self {
            PropertyValue::Checkbox { checkbox, .. } => Ok(*checkbox),
            PropertyValue::Formula {
                formula: FormulaResultValue::Boolean { boolean },
                ..
            } => boolean.ok_or_else(|| {
                err!(AppErr::Value(
                    "found Formula but its either empty or not a checkbox computation".to_owned()
                ))
            }),
            _ => Err(self.err_prop_type("checkbox")),
        }
    }

    fn as_select<E>(&self) -> AppResult<E>
    where
        E: Copy + FromStr,
    {
        match self {
            PropertyValue::Select { select, .. } => match select {
                Some(v) if v.name.is_some() => {
                    v.name.as_ref().unwrap().parse::<E>().or_else(|_| {
                        Err(err!(AppErr::Value(format!(
                            "{} is not a valid enum variant",
                            v.name.as_ref().unwrap()
                        ))))
                    })
                }
                Some(_) => Err(err!(AppErr::Value("empty new (select)".to_owned()))),
                None => Err(err!(AppErr::Value("empty (select)".to_owned()))),
            },
            _ => Err(self.err_prop_type("select")),
        }
    }

    fn as_multi_select<E>(&self) -> AppResult<Vec<E>>
    where
        E: Copy + FromStr,
    {
        match self {
            PropertyValue::MultiSelect { multi_select, .. } => match multi_select {
                Some(values) => {
                    let mut enums = Vec::new();
                    let mut invalid_values = Vec::new();
                    for value in values {
                        match &value.name {
                            Some(name) => match name.parse::<E>() {
                                Ok(e) => enums.push(e),
                                Err(_) => invalid_values.push(name.clone()),
                            },
                            None => panic!("??? - notion update - found empty select name"), // invalid_values.push("EMPTY")
                        }
                    }
                    match invalid_values.is_empty() {
                        true => Ok(enums),
                        false => Err(err!(AppErr::Value(format!(
                            "{} are not valid enum variants",
                            invalid_values.join(", ")
                        )))),
                    }
                }
                None => Err(err!(AppErr::Value("empty (multi-select)".to_owned()))),
            },
            _ => Err(self.err_prop_type("multi_select")),
        }
    }

    fn as_relation(&self) -> AppResult<PageId> {
        match self {
            PropertyValue::Relation { relation: None, .. } => Err(err!(AppErr::Value(
                "expected a single relation, found none".to_owned()
            ))),
            PropertyValue::Relation {
                relation: Some(relations),
                ..
            } if relations.len() != 1 => Err(err!(AppErr::Value(format!(
                "expected a single relation, found {}",
                relations.len()
            )))),
            PropertyValue::Relation {
                relation: Some(relations),
                ..
            } => Ok(relations[0].id.clone()),
            _ => return Err(self.err_prop_type("relations")),
        }
    }

    fn as_relations(&self) -> AppResult<Vec<PageId>> {
        match self {
            PropertyValue::Relation { relation: None, .. } => Ok(Vec::new()),
            PropertyValue::Relation {
                relation: Some(relations),
                ..
            } => Ok(relations
                .iter()
                .map(|relation| relation.id.clone())
                .collect()),
            _ => return Err(self.err_prop_type("relations")),
        }
    }

    fn as_relations_pages(&self, pages: &Vec<Page>) -> AppResult<Vec<Page>> {
        match self {
            PropertyValue::Relation { relation: None, .. } => Ok(Vec::new()),
            PropertyValue::Relation {
                relation: Some(relations),
                ..
            } => {
                let result: Vec<Page> = pages
                    .iter()
                    .filter(|page| relations.iter().any(|relation| relation.id == page.id))
                    .cloned()
                    .collect();
                if result.len() != relations.len() {
                    return Err(err!(AppErr::Value(format!(
                        "bug: {} relations (out of {}) didn't find their match ({})",
                        relations.len() - result.len(),
                        relations.len(),
                        pages.iter().filter_map(|p| p.title()).collect::<String>()
                    ))));
                }
                Ok(result)
            }
            _ => return Err(self.err_prop_type("relations pages")),
        }
    }
}

pub trait PageProp {
    fn as_text_array(&self, prop: &str) -> AppResult<Vec<String>>;
    fn as_text(&self, prop: &str) -> AppResult<String>;
    fn as_f64(&self, prop: &str) -> AppResult<f64>;
    fn as_i64(&self, prop: &str) -> AppResult<i64>;
    fn as_u64(&self, prop: &str) -> AppResult<u64>;
    fn as_bool(&self, prop: &str) -> AppResult<bool>;
    fn as_select<E>(&self, prop: &str) -> AppResult<E>
    where
        E: Copy + FromStr;
    fn as_multi_select<E>(&self, prop: &str) -> AppResult<Vec<E>>
    where
        E: Copy + FromStr;
    fn as_relation(&self, prop: &str) -> AppResult<PageId>;
    fn as_relations(&self, prop: &str) -> AppResult<Vec<PageId>>;
    fn as_relations_pages(&self, prop: &str, pages: &Vec<Page>) -> AppResult<Vec<Page>>;
    fn as_relation_content<T>(&self, prop: &str, db: &Table<T>) -> AppResult<T>
    where
        T: Sized
            + std::fmt::Debug
            + Clone
            + PartialEq
            + Serialize
            + for<'a> Deserialize<'a>
            + for<'a> TryFrom<(&'a Page, &'a Tables)>
            + Validate
            + TableOutput,
        for<'a> <T as TryFrom<(&'a Page, &'a Tables)>>::Error: ToString;

    fn as_relations_content<T>(&self, prop: &str, db: &Table<T>) -> AppResult<Vec<T>>
    where
        T: Sized
            + std::fmt::Debug
            + Clone
            + PartialEq
            + Serialize
            + for<'a> Deserialize<'a>
            + for<'a> TryFrom<(&'a Page, &'a Tables)>
            + Validate
            + TableOutput,
        for<'a> <T as TryFrom<(&'a Page, &'a Tables)>>::Error: ToString;

    fn prefix_err_db_entry_title(mut errors: AppErrors, title: Option<String>) -> AppErrors {
        let last = errors.last_mut().unwrap();
        let new_msg = format!(
            "{} :: {}",
            title.unwrap_or("?".to_owned()),
            last.get_message()
        );
        last.replace(new_msg);
        errors
    }
}

impl PageProp for Page {
    fn as_text_array(&self, prop: &str) -> AppResult<Vec<String>> {
        Api::get(self, prop)?
            .as_text_array()
            .or_else(|errors| Err(Self::prefix_err_db_entry_title(errors, self.title())))
    }

    fn as_text(&self, prop: &str) -> AppResult<String> {
        Api::get(self, prop)?
            .as_text()
            .or_else(|errors| Err(Self::prefix_err_db_entry_title(errors, self.title())))
    }

    fn as_f64(&self, prop: &str) -> AppResult<f64> {
        Api::get(self, prop)?
            .as_f64()
            .or_else(|errors| Err(Self::prefix_err_db_entry_title(errors, self.title())))
    }

    fn as_i64(&self, prop: &str) -> AppResult<i64> {
        Api::get(self, prop)?
            .as_i64()
            .or_else(|errors| Err(Self::prefix_err_db_entry_title(errors, self.title())))
    }

    fn as_u64(&self, prop: &str) -> AppResult<u64> {
        Api::get(self, prop)?
            .as_u64()
            .or_else(|errors| Err(Self::prefix_err_db_entry_title(errors, self.title())))
    }

    fn as_bool(&self, prop: &str) -> AppResult<bool> {
        Api::get(self, prop)?
            .as_bool()
            .or_else(|errors| Err(Self::prefix_err_db_entry_title(errors, self.title())))
    }

    fn as_select<E>(&self, prop: &str) -> AppResult<E>
    where
        E: Copy + FromStr,
    {
        Api::get(self, prop)?
            .as_select()
            .or_else(|errors| Err(Self::prefix_err_db_entry_title(errors, self.title())))
    }

    fn as_multi_select<E>(&self, prop: &str) -> AppResult<Vec<E>>
    where
        E: Copy + FromStr,
    {
        Api::get(self, prop)?
            .as_multi_select()
            .or_else(|errors| Err(Self::prefix_err_db_entry_title(errors, self.title())))
    }

    fn as_relation(&self, prop: &str) -> AppResult<PageId> {
        Api::get(self, prop)?
            .as_relation()
            .or_else(|errors| Err(Self::prefix_err_db_entry_title(errors, self.title())))
    }

    fn as_relation_content<T>(&self, prop: &str, db: &Table<T>) -> AppResult<T>
    where
        T: Sized
            + std::fmt::Debug
            + Clone
            + PartialEq
            + Serialize
            + for<'a> Deserialize<'a>
            + for<'a> TryFrom<(&'a Page, &'a Tables)>
            + Validate
            + TableOutput,
        for<'a> <T as TryFrom<(&'a Page, &'a Tables)>>::Error: ToString,
    {
        Api::get(self, prop)?
            .as_relation()
            .or_else(|errors| Err(Self::prefix_err_db_entry_title(errors, self.title())))
            .and_then(|page_id| db.get(&page_id))
    }

    fn as_relations(&self, prop: &str) -> AppResult<Vec<PageId>> {
        Api::get(self, prop)?
            .as_relations()
            .or_else(|errors| Err(Self::prefix_err_db_entry_title(errors, self.title())))
    }

    fn as_relations_content<T>(&self, prop: &str, db: &Table<T>) -> AppResult<Vec<T>>
    where
        T: Sized
            + std::fmt::Debug
            + Clone
            + PartialEq
            + Serialize
            + for<'a> Deserialize<'a>
            + for<'a> TryFrom<(&'a Page, &'a Tables)>
            + Validate
            + TableOutput,
        for<'a> <T as TryFrom<(&'a Page, &'a Tables)>>::Error: ToString,
    {
        Api::get(self, prop)?
            .as_relations()
            .or_else(|errors| Err(Self::prefix_err_db_entry_title(errors, self.title())))?
            .iter()
            .map(|page_id| db.get(&page_id))
            .collect::<AppResult<Vec<T>>>()
    }

    fn as_relations_pages(&self, prop: &str, pages: &Vec<Page>) -> AppResult<Vec<Page>> {
        Api::get(self, prop)?
            .as_relations_pages(pages)
            .or_else(|errors| Err(Self::prefix_err_db_entry_title(errors, self.title())))
    }
}
