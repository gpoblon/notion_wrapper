use crate::{
    result::{err, AppErr, AppErrors, AppResult, ErrorTrait},
    types::{
        DbName, GameplayAbility, GameplayEffect, GameplayTag, GameplayTagAndLocalization,
        PackedEffect,
    },
    NotionApi,
};
use notion::{chrono::NaiveDateTime, ids::PageId, models::Page};
use semver::Version;
use serde::{Deserialize, Serialize};
use serde_json::error;
use std::collections::HashMap;
use validator::{Validate, ValidationErrors};

pub trait NotionTable<T, Rhs = Self>
where
    T: for<'a> TryFrom<(&'a Page, &'a NotionTables)> + Validate,
    for<'a> <T as TryFrom<(&'a Page, &'a NotionTables)>>::Error: ToString,
    Self: Sized + Serialize + for<'a> Deserialize<'a>,
{
    fn get(&self, page: &PageId) -> AppResult<T>;

    fn build(
        content: HashMap<PageId, T>,
        version: Option<Version>,
        last_edited_time: Option<NaiveDateTime>,
    ) -> Self;

    async fn is_up_to_date(&self, notion_api: NotionApi, name: DbName) -> AppResult<bool>;

    async fn try_build(
        notion_api: &NotionApi,
        tables: &NotionTables,
        db: DbName,
        current_version: Version,
        current_last_edited_time: NaiveDateTime,
    ) -> AppResult<Self> {
        log::info!("trying to build {}...", db);
        log::info!("querying notion api...");
        let mut version: Option<Version>;
        let mut last_edited_time: Option<NaiveDateTime>;
        match notion_api.query_database(db).await {
            Ok(response) => {
                log::info!("transmuting notion result into struct (parsing + full validation)...");
                let mut content: HashMap<PageId, T> = HashMap::new();
                let mut errors = AppErrors::default();
                for page in &response.pages {
                    match T::try_from((page, tables)) {
                        Ok(elem) => match elem.validate() {
                            Ok(_) => {
                                content.insert(page.id.to_owned(), elem);
                            }
                            Err(e) => {
                                errors.push(AppErr::Validation(e));
                            }
                        },
                        Err(e) => errors.push(AppErr::Deserialization(e.to_string())),
                    };
                }
                if !errors.is_empty() {
                    return Err(errors);
                }
                Ok(Self::build(
                    content,
                    response.metadata.version,
                    response.metadata.last_edited_time,
                ))
            }
            Ok(_) => Err(err!(AppErr::Notion("database is empty".to_owned()))),
            Err(e) => Err(err!(AppErr::Notion(e.to_string()))),
        }
    }
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct NotionTables {
    pub tags: NotionTags,
    pub abilities: NotionAbilities,
    pub effects: NotionEffects,
    pub packed_effects: NotionPackedEffects,
    // pub items: NotionItems,
    // pub interactables: NotionInteractables,
    // pub spawnables: NotionSpawnables,
    // pub companions: NotionCompanions,
    // pub recipes: NotionRecipes,
}
impl NotionTables {
    pub fn build_from_file() -> AppResult<Self> {
        log::info!("build from file...");
        let content = std::fs::read_to_string(format!("../data/notion_dbs.json"))
            .map_err(|e| err!(AppErr::Db(e.to_string())))?;
        serde_json::from_str::<Self>(&content).map_err(|e| err!(AppErr::Db(e.to_string())))
    }

    pub async fn build(notion_api: NotionApi) -> AppResult<Self> {
        let mut tables = NotionTables::build_from_file().log().unwrap_or_default();
        if !tables
            .tags
            .is_up_to_date(notion_api, DbName::GameplayTags)
            .await?
        {
            tables.tags = NotionTags::try_build(&notion_api, &tables, DbName::GameplayTags).await?;
        }
        // tables.abilities = NotionAbilities::try_build(&notion_api, &tables, DbName::Abilities).await?.to_file(DbName::Abilities);
        Ok(tables)
    }

    pub fn to_file(&self) {
        let output = format!("../data/notion_dbs.json");
        std::fs::write(&output, serde_json::to_string_pretty(self).unwrap());
        log::info!(
            "validated content of all notion databases have been written to {}",
            output
        );
    }
}

/*
   Macro that declares a new struct following the same pattern
   All classes are built the same way, and since rust doesn't have inheritance, this cuts through boilerplate
*/
macro_rules! notion_db_class_builder {
    (struct $name: ident, $content: ty) => {
        #[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
        pub struct $name {
            last_edited_time: Option<NaiveDateTime>,
            version: Option<Version>,
            content: HashMap<PageId, $content>,
        }

        impl NotionTable<$content> for $name {
            fn build(
                content: HashMap<PageId, $content>,
                version: Option<Version>,
                last_edited_time: Option<NaiveDateTime>,
            ) -> Self {
                Self {
                    last_edited_time,
                    version,
                    content,
                }
            }

            fn get(&self, page: &PageId) -> AppResult<$content> {
                match self.content.get(page) {
                    Some(e) => Ok(e.to_owned()),
                    None => Err(err!(AppErr::Db(format!(
                        "could not find '{}' in parsed {} db",
                        stringify!($name),
                        page
                    )))),
                }
            }

            async fn is_up_to_date(&self, notion_api: NotionApi, name: DbName) -> AppResult<bool> {
                match notion_api.query_database_metadata(name).await {
                    Ok(response) => Ok(self.version == response.version
                        && self.last_edited_time == response.last_edited_time),
                    Err(e) => Err(err!(AppErr::Notion(e.to_string()))),
                }
            }
        }
    };
}

notion_db_class_builder!(struct NotionPackedEffects, PackedEffect);
notion_db_class_builder!(struct NotionEffects, GameplayEffect);
notion_db_class_builder!(struct NotionAbilities, GameplayAbility);
notion_db_class_builder!(struct NotionTags, GameplayTagAndLocalization);
impl NotionTags {
    pub fn as_tag(&self, page: &PageId) -> AppResult<GameplayTag> {
        self.get(page).and_then(|tag| Ok(tag.as_tag()))
    }

    pub fn get_name(&self, page: &PageId) -> AppResult<String> {
        self.get(page).and_then(|tag| Ok(tag.get_name()))
    }

    pub fn get_desc(&self, page: &PageId) -> AppResult<String> {
        self.get(page).and_then(|tag| Ok(tag.get_description()))
    }
}
