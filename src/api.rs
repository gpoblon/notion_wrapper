// discards warnings as file is not used up to its full potential
use http::HeaderValue;
use http::{method::Method, HeaderMap};
use lazy_static::lazy_static;
use notion::chrono::NaiveDateTime;
use notion::ids::DatabaseId;
use notion::models::paging::{Pageable, PagingCursor};
use notion::models::properties::PropertyValue;
use notion::models::search::DatabaseQuery;
use notion::models::{ListResponse, Page};
use notion::NotionApi as ExternalNotionApi;
use oauth2::{reqwest::async_http_client, url::Url, AccessToken, HttpRequest, HttpResponse};
use regex::Regex;
use semver::Version;
use serde::de::DeserializeOwned;
use serde::Deserialize;
use serde::Serialize;
use std::collections::BTreeMap;
use std::fs::File;
use std::io::Read;
use toml;

pub type Result<P> = core::result::Result<P, String>;

pub type TableName = String;
#[derive(Clone, Deserialize)]
pub struct Tables(BTreeMap<TableName, DatabaseId>);
impl Tables {
    fn get_id(&self, db: &str) -> Result<&DatabaseId> {
        match self.get(db) {
            Some(id) => Ok(id),
            None => Err(format!("No database id matches {db}")),
        }
    }

    fn get_name_from_id(&self, id: &str) -> Result<&TableName> {
        match self.iter().find(|(_, db_id)| id == &db_id.to_string()) {
            Some((name, _)) => Ok(name),
            None => Err(format!("No database name matches {id}")),
        }
    }
}
impl core::ops::Deref for Tables {
    type Target = BTreeMap<TableName, DatabaseId>;

    fn deref(self: &'_ Self) -> &'_ Self::Target {
        &self.0
    }
}

// load .env file
#[derive(Deserialize)]
struct Connector {
    notion: Api,
}
impl Connector {
    pub fn build() -> Result<Self> {
        let mut file: File = File::open(".env").map_err(|e| e.to_string())?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)
            .map_err(|e| e.to_string())?;
        toml::from_str::<Connector>(&contents).map_err(|e| e.to_string())
    }
}

pub struct TableMetadata {
    pub last_edited_time: Option<NaiveDateTime>,
    pub version: Option<Version>,
}

pub struct TableQuery {
    pub metadata: TableMetadata,
    pub pages: Vec<Page>,
}

#[derive(Deserialize, Clone)]
pub struct Api {
    url: Url,
    token: AccessToken,
    version: Version,
    #[serde(skip_deserializing)]
    api: Option<ExternalNotionApi>,
    pub databases: Tables,
}
impl Api {
    pub async fn connect(&mut self) -> Result<()> {
        ExternalNotionApi::new(self.token.secret().to_owned()).map_err(|e| e.to_string())?;
        Ok(())
    }

    // simpler but heavier and requires a .env file.
    // Prefer using connect() if other configuration is required.
    pub async fn build() -> Result<Self> {
        let mut connector = Connector::build().map_err(|e| e.to_string())?.notion;
        connector.api = Some(
            ExternalNotionApi::new(connector.token.secret().to_owned())
                .map_err(|e| e.to_string())?,
        );
        Ok(connector)
    }

    pub fn get_api(&self) -> Result<&ExternalNotionApi> {
        match &self.api {
            Some(api) => Ok(api),
            None => Err("undefined Api (probably a connector data error)".to_owned()),
        }
    }

    pub fn get_property_value(page: &Page, property: &str) -> Result<PropertyValue> {
        match page.properties.properties.get(property) {
            Some(v) => Ok(v.to_owned()),
            None => Err(format!(
                "property '{}' not found in {:?}",
                property,
                page.title()
            )),
        }
    }

    fn get_version_from_str(str: &str) -> Option<Version> {
        lazy_static! {
            static ref RE_VERSION: Regex = Regex::new(r#"\d+\.\d+\.\d+"#).unwrap();
        }
        let cap = RE_VERSION.captures(str)?.get(0)?.as_str();
        Version::parse(cap).ok()
    }

    pub async fn query_database_metadata(&self, table: &TableName) -> Result<TableMetadata> {
        log::info!("getting notion database metadata");

        let db = self
            .get_api()?
            .get_database(self.databases.get_id(&table)?)
            .await
            .map_err(|e| e.to_string())?;
        Ok(TableMetadata {
            last_edited_time: Some(db.last_edited_time.naive_utc()),
            version: Self::get_version_from_str(&db.title_plain_text()),
        })
    }

    pub async fn query_database_pages(&self, route: &TableName) -> Result<Vec<Page>> {
        let mut pages = Vec::new();
        let mut cursor = None;
        let id = self.databases.get_id(&route)?;
        log::info!("querying {} table", route);

        loop {
            let result = self.query_next(id, cursor).await;
            match result {
                Ok(mut response) => {
                    pages.append(&mut response.results);
                    if !response.has_more {
                        break;
                    }
                    log::info!("... a hundred more pages");
                    cursor = response.next_cursor;
                }
                Err(e) => return Err(e.to_string()),
            }
        }
        log::info!(
            "{} table loaded with {} pages to transmute",
            route,
            pages.len()
        );
        Ok(pages)
    }

    async fn query_next(
        &self,
        id: &DatabaseId,
        cursor: Option<PagingCursor>,
    ) -> Result<ListResponse<Page>> {
        let response = self
            .get_api()?
            .query_database(id, DatabaseQuery::default().start_from(cursor.clone()))
            .await
            .map_err(|e| e.to_string())?;
        if response.results.len() > 0 {
            Ok(response)
        } else {
            Err(format!("db is empty (first call?: {})", cursor.is_none()))
        }
    }

    pub async fn query_database(&self, route: &TableName) -> Result<TableQuery> {
        Ok(TableQuery {
            metadata: self.query_database_metadata(route).await?,
            pages: self.query_database_pages(route).await?,
        })
    }

    pub async fn request<T>(&self, method: Method, route: &TableName) -> Result<T>
    where
        T: DeserializeOwned + Serialize,
    {
        let req = self.req::<T>(method, route, None)?;
        let response = Self::exec_request(&req).await?;
        match &response.headers.get("X-Total") {
            None => {
                // let v: serde_json::Value = serde_json::from_slice(&response.body).unwrap();
                // log::info!("{}", serde_json::to_string_pretty(&v).unwrap());
                Self::parse_response(response)
            }
            _ => Err("Expected a single response value, got multiple".to_owned()),
        }
    }

    pub async fn request_payload<T>(
        &self,
        method: Method,
        table: &TableName,
        payload: T,
    ) -> Result<()>
    where
        T: DeserializeOwned + Serialize,
    {
        let req = self.req::<T>(method, table, Some(payload))?;
        let _response = Self::exec_request(&req).await?;
        Ok(())
    }

    fn database_route(&self, route: &TableName) -> Result<String> {
        Ok(format!(
            "/databases/{}/query",
            self.databases.get_id(route)?
        ))
    }

    fn req<T>(&self, method: Method, route: &TableName, payload: Option<T>) -> Result<HttpRequest>
    where
        T: DeserializeOwned + Serialize,
    {
        let body: Vec<u8> = match payload {
            Some(p) => {
                log::info!("{}", serde_json::to_string_pretty(&p).unwrap());
                match serde_json::to_string(&p) {
                    Ok(s) => s.as_bytes().to_owned(),
                    Err(e) => return Err(e.to_string()),
                }
            }
            None => Vec::new(),
        };

        let url: Url = Url::parse(&format!("{}{}", self.url, self.database_route(route)?))
            .map_err(|e| format!("could not parse url: {}", e.to_string()))?;

        let mut request = HttpRequest {
            url,
            method: method.clone(),
            headers: HeaderMap::new(),
            body,
        };

        request.headers.insert(
            http::header::AUTHORIZATION,
            http::HeaderValue::from_str(&format!("Bearer {}", self.token.secret())).unwrap(),
        );
        request.headers.insert(
            "Notion-Version",
            HeaderValue::from_str(&self.version.to_string()).map_err(|e| e.to_string())?,
        );
        request.headers.insert(
            http::header::ACCEPT,
            http::HeaderValue::from_str("application/json").unwrap(),
        );
        if method != Method::GET {
            request.headers.insert(
                http::header::CONTENT_TYPE,
                http::HeaderValue::from_str("application/json").unwrap(),
            );
        }
        Ok(request)
    }

    async fn exec_request(request: &HttpRequest) -> Result<HttpResponse> {
        let response = async_http_client(request.clone())
            .await
            .map_err(|_e| "request failed".to_owned())?;
        log::info!("{}", request.url);

        if response.status_code != 200 {
            return Err(format!(
                "Request failed: {} -> {:?}",
                response.status_code, request
            ));
        }
        Ok(response)
    }

    fn parse_response<T>(response: HttpResponse) -> Result<T>
    where
        T: DeserializeOwned + Serialize,
    {
        serde_json::from_str(&String::from_utf8_lossy(response.body.as_slice()))
            .or(serde_json::from_str(&format!(
                r#"{{"status_code": "{}", "body": "{}"}}"#,
                response.status_code,
                String::from_utf8_lossy(&response.body)
            )))
            .map_err(|e| {
                format!(
                    r#"{{"status_code": "{}", "body": "{}"}}"#,
                    response.status_code,
                    e.to_string()
                )
            })
    }
}
